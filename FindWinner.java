
public class FindWinner {
	
	static String winner(int[] andrea, int[] maria, String s) {
		int n = andrea.length;
		int and_points = 0, maria_points = 0, start =0;
		if(s.equals("Odd"))
			start = 1;
		for(int i = start;i<n;i+=2){
			and_points += andrea[i] - maria[i];
			maria_points +=  maria[i]- andrea[i];
		}
		
		if(and_points > maria_points)
			return "Andrea";
		else if(and_points < maria_points)
			return "Maria";
		
		return "Tie";
	}
		
	public static void main(String arg[]){
		int [] andrea = {1,2,3};
		int [] maria = {2,1,3};
		String s = "Even";
		System.out.println(winner(andrea,maria,s));
	}

}
