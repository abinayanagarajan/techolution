
public class PsychometricTesting {
	
	static int[] jobOffers(int[] scores, int[] lowerLimits, int[]
			upperLimits) {
		int canditates[]=new int[lowerLimits.length];
		for(int i=0;i<lowerLimits.length;i++){
			 int selectedCanditates = 0;
			for(int j=0;j<scores.length;j++){
				if(scores[j]>=lowerLimits[i] && scores[j]<=upperLimits[i]){
					selectedCanditates++;
				}
			}
			canditates[i] = selectedCanditates;
		}	
		return canditates;
	}
	public static void main(String[] args) {
		//custom input
		int[] scores = {4,8,7};
		int[] lowerLimits = {2,4};
		int[] upperLimits = {8,4};
		int [] canditatesOffered = jobOffers(scores,lowerLimits, upperLimits);
		for(int i : canditatesOffered){
			System.out.println(i);
		}	
    }
}
