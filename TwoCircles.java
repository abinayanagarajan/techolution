
public class TwoCircles {

	static String[] circles(String[] info) {
		String[] result = new String[info.length];
		for(int i=0;i<info.length;i++){
			String[] columns = info[i].split(" "); 
			int xA = Integer.parseInt(columns[0]);
			int yA = Integer.parseInt(columns[1]);
			int rA = Integer.parseInt(columns[2]);
			int xB = Integer.parseInt(columns[3]);
			int yB = Integer.parseInt(columns[4]);
			int rB = Integer.parseInt(columns[5]);
			double dCenter;
			
			if((xA == xB) && (yA == yB)){
				result[i] ="Concentric";	
				continue;
			}
			
			//distance between two centers
			dCenter = Math.sqrt(((xB - xA)*(xB - xA))+((yB - yA)*(yB - yA)) );
			if(dCenter == (rA+rB)||dCenter == Math.abs(rA-rB)){
				result[i] = "Touching";
				continue;
			}				
			if(dCenter <= (rA+rB)&& dCenter >= Math.abs(rA-rB)){
				result[i] = "Intersecting";
				continue;
			}
			if(dCenter > (rA+rB)){
				result[i] = "Disjoint-Outside";
				continue;
			}
			if(dCenter < Math.abs(rA-rB)){
				result[i] = "Disjoint-Inside";
				continue;
			}			
		}
		return result;
	}

	public static void main(String arg[]){
		String[] info={"0 5 9 0 9 7",
				"0 15 11 0 20 16",
				"26 0 10 39 0 23",
				"37 0 5 30 0 11",
				"2 2 4 4 4 1"};
		String[] res = circles(info);
		for(String r : res){
			System.out.println(r);
		}	
		
	}
	
}
