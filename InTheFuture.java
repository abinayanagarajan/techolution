public class InTheFuture {

	static int minNum(int a, int k, int p) {
		//case where Kelly can never go ahead of Asha
		if(a > k && p >= 0)
			return -1;
		else if (a < k && p == 0)
			return 1;
		int asha = a+p;
		int kelly = k;
		int flag = 1;
		while(asha >= kelly){
			asha = asha + a;
			kelly = kelly + k;
			flag++;			
		}
		return flag;
	}
	public static void main(String arg[]){		
		System.out.println(minNum(4,50,10));
	}
	
}
