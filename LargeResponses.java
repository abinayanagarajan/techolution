import java.io.*;
public class LargeResponses
{
  public static void main(String[] args)throws Exception
  {
  File inputFile = new File("AbsoluteFolderPath\\Test.txt"); 
  BufferedReader br = new BufferedReader(new FileReader(inputFile));
  String st;
  int num = 0, totalBytes = 0;
  while ((st = br.readLine()) != null){
	  String[] columns = st.split(" ");
	  int bytes = Integer.parseInt(columns[columns.length-1]);
	  if(bytes > 5000){
		  num ++;
		  totalBytes += bytes;
	  }
  }
 // System.out.println(num + " " + totalBytes);
  File outputFile = new File("AbsoluteFolderPath\\TestResult.txt"); 
  BufferedWriter bw = new BufferedWriter(new FileWriter(outputFile));
  bw.write(Integer.toString(num));
  bw.newLine();
  bw.write(Integer.toString(totalBytes));
  bw.close();
  br.close();
  System.out.println("Done!");
  }
}