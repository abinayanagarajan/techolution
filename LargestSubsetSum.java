public class LargestSubsetSum{
    static long[] maxSubsetSum(int[] k)
    {
        //k should be ideally be a long array
        long ret[] = new long[k.length];
        for(int x=0;x<k.length;x++)
        {
            long sum=0;
            long inp = Long.valueOf(k[x]);
            long max = (int)Math.sqrt(inp);
            for(long num = 1; num <= max; num++)
            {
                if(inp%num == 0)
                {
                   sum = sum + num;
                   long div = inp/num;
                   if(div!=num)
                   {
                       sum+=div;
                   }
                }
            }
            ret[x] = sum;
        }
        return ret;
    }
    
    public static void main (String[] args)
    {
        int[] inp = {2,4};
        long[] re = new long[inp.length];
        re = maxSubsetSum(inp);
        for(long x : re)
        {
            System.out.println(x);
        }
    }
}