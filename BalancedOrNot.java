public class BalancedOrNot {

    static int[] balancedOrNot(String[] expressions, int[] maxReplacements) {

        int[] balancedPossible = new int[expressions.length];
        for (int i = 0; i < expressions.length; i++) {
            char[] items = expressions[i].toCharArray();
            balancedPossible[i] = isTagBalanced(items, maxReplacements[i]);

        }
        return balancedPossible;
    }

    static int isTagBalanced(char[] items, int m) {
        stack s = new stack();
        for (int j = 0; j < items.length; j++) {
            if (items[j] == '<') {
                s.push(items[j]);
            }
            if (items[j] == '>') {
                s.pop();
            }
        }
        //if it is already balanced
        if (s.top == -1)
            return 1;
        //checking for maximum replacements
        if (s.startTag > 0)
            return 0;
        else if (s.startTag == 0 && s.lastTag <= m)
            return 1;

        return 0;
    }

    static class stack {
        int top = -1;
        int startTag = 0, lastTag = 0;
        char[] item = new char[10000];
        void push(char a) {
            if (top == 9999) {
                //System.out.println("Stack full");
            } else {
                item[++top] = a;
                startTag++;
            }
        }
        void pop() {
            if (top == -1 || startTag == 0) {
                item[++top] = '>';
                lastTag++;
            } else {
                top--;
                startTag--;
                //return a;
            }
        }
    }
    public static void main(String arg[]) {
        String[] r = {
            "<>>",
            "><><>>><>"
        };
        int[] m = {2,3};
        int[] b = (balancedOrNot(r, m));
        for (int i: b) {
            System.out.println(i);
        }
    }
}