public class ConsecutiveSum
{    
    static int consecutive(long  N)
    {
        int count = 0;
        for (int i=1; i*(i + 1)< 2*N; i++)
        {
            float consGroup = (float)((1.0*N-(i*(i+1))/2)/(i+1));
            if (consGroup-(int)consGroup == 0.0) 
                count++;        
        }
        return count;
    }

    public static void main(String[] args) {
        System.out.println(consecutive(15));
    }
}