import java.util.*;

public class BuyingShowTicket {
	
	static long waitingTime(int[] tickets, int p) {
		long queuetime = 0;
        List<Integer> ticketList = new ArrayList<Integer>();
        for (int i = 0; i < tickets.length; i++) {
        	ticketList.add(tickets[i]);
        }
        boolean done = false;
        while (!done) {
            for (int i = 0; i < ticketList.size(); i++) {
                if (ticketList.get(p) == 0) {
                    done = true;
                    break;
                }
                if (ticketList.get(i) == 0) {
                    continue;
                }
                ticketList.set(i, ticketList.get(i) - 1);
                queuetime++;
            }
        }
        return queuetime;
    }
	
	public static void main(String arg[]){
		int [] tickets = {2,6,3,4,5};
		int p = 2;
		System.out.println(waitingTime(tickets,p));
	}

}
